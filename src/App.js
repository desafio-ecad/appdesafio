import './App.css';
import {Component} from "react";

import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";

class App extends Component {

  callApi = () =>{
    const urlBase = 'http://localhost:8080/?';
    let urlFinal="";
   
     urlFinal = urlBase+'name='+this.state.nome+"&"+'file_type='+this.state.tipo+"&"+'created_at_start='+this.state.dataInicial+"&"+'created_at_end='+this.state.dataFinal ;
  

  console.log(urlFinal);
     fetch(urlFinal)
      .then(async response => {
        const data = await response.json();
        if (!response.ok) {
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        this.setState({data: data, isFetching: false});      
      })
      .catch(error => {
          this.setState({errorMessage: error.toString(), isFetching: false});
      });
  }

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
        isFetching: true,
        data: [{}],
        nome:"", tipo:"", dataInicial:"", dataFinal:""
    };
  
  }

handleChange(event) {
    this.setState({tipo: event.target.value});
  }

setarDataInicial=(e)=>{
  console.log(e);
this.setState({dataInicial:e.format('YYYY-MM-DD')});    
}

setarDataFinal=(e)=>{
this.setState({dataFinal:e.format('YYYY-MM-DD')});   
}

myChangeHandler = (event) => {
    this.setState({nome: event.target.value});
}


  render() {
    
    return (
      <div>
        <p> Nome</p>
        <input
        type='text'
        onChange={this.myChangeHandler}
         />
      
         <p> Tipo</p>
         <select value={this.state.tipo} onChange={this.handleChange}>
         <option value="" selected>Todos</option>
          <option value="REMESSA">REMESSA</option>
          <option value="RETORNO">RETORNO</option>
         </select>
       
      
        <p> Data Inicial</p>
        <Datetime onChange={this.setarDataInicial} value={this.state.dataInicial}/>
        <p> Data Final</p>
        <Datetime onChange={this.setarDataFinal} value={this.state.dataFinal} />

        <p></p>
       <button onClick={this.callApi}>Pesquisar</button>

        {
          (this.state.isFetching)
          ?
            <div></div>
          : 

      <div>
           
         <p></p>
        <table>        
          {this.state.data.map(obj => (
            <tr>
            <td key={obj.name}>{obj.name}</td>
            <td key={obj.type}>{obj.type}</td>
            <td key={obj.createdAt}>{obj.createdAt}</td>
            <td key={obj.qtdLines}>{obj.qtdLines}</td>
            <td key={obj.value}>{obj.value}</td>
            
            </tr>
          ))}
        </table>
        </div>
         }
      </div>
    ) 
  }      
}

export default App;
